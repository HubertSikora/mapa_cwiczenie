package sda.example;

import java.util.List;

public class App {
    public static void main(String[] args) {
        //       demo1();
        demo2();
        demo3mapa();
    }

    /*
        // metoda haszmapy bezpośrednio w mainie
        public static void demo1() {
            // tworzenie obiektu mapa
            Map<String, String> mapaDemo = new HashMap<>();
            // wrzucenie do mapy pary klucz-wartosc
            mapaDemo.put("Cześć", "Hello");
            mapaDemo.put("Do widzenia", "Goodbye");
            mapaDemo.put("Budynek", "Building");
    // wywoływanie forem wszystkich wartości z mapy
            for (String key : mapaDemo.keySet()) {
                System.out.println("for: " + mapaDemo.get(key));
            }
            //wypisanie
                 System.out.println(mapaDemo.get("Cześć"));
            }
        */
/*
    // inna wersja - LinkedHaszMap

        Map<String, String> mapaDemo2 = new LinkedHashMap<>();
        mapaDemo2.put("Cześć","Hello");
        mapaDemo2.put("Do widzenia", "Goodbye");
        mapaDemo2.put("Budynek", "Building");
        //
        for (String key  : mapaDemo2.keySet()) {
            System.out.println(mapaDemo2.get(key));
        }
    }
*/
    // klasa osobno - przeniesione wyżej
    public static void demo2() {
        Dictionary dictionary = new Dictionary();
        String translation1 = dictionary.translate("car");
        System.out.println(translation1);

        List<String> translation2 = dictionary.translateAsList("glas");
        System.out.println(translation2);

    }


    // metoda
    public static void demo3mapa() {
        DictionarySimple dictionarySimple = new DictionarySimple();
        String tłumaczenie = dictionarySimple.translate("Hello");
        String tłumaczenie2 = dictionarySimple.translate("School");
        System.out.println(tłumaczenie);
        System.out.println(tłumaczenie2);


    }
}
