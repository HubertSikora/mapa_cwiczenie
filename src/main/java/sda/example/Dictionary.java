package sda.example;

import java.util.*;

public class Dictionary {
    private Map<String, List<String>> dictionary = new HashMap<>();

    public Dictionary() {
// Podejscie 1)
        List<String> samochodTranslator = new ArrayList<>();
        samochodTranslator.add("samochodzik");
        samochodTranslator.add("auto");
        dictionary.put("car", samochodTranslator); //paraowanie klucza-car z ArrayListą słów
//Podejście 2)
        //tworzy tablice stringów i odrazu w niej zapisuje słowa(wartosci)
        String[] glasTranslator = new String[]{"szklanka", "okno"};
        // parowanie klucza-glas , tworzy ArrayListe i .. ?
        dictionary.put("glas", new ArrayList<>(Arrays.asList(glasTranslator)));
    }

// gettery
    public String translate(String wordToTranslate) {
        return dictionary.get(wordToTranslate.toLowerCase()).toString();
    }

    public List<String> translateAsList(String wordToTranslate) {
        return dictionary.get(wordToTranslate.toLowerCase());
    }

}
