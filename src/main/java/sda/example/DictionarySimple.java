package sda.example;


import java.util.HashMap;
import java.util.Map;
//klasa z prostym słownikiem
public class DictionarySimple {

    // utworzenie obiektu mapy
    private Map<String, String> dict = new HashMap<>();
// konstruktor
    public DictionarySimple() {
        dict.put("Hello", "Cześć");
        dict.put("Goodbye", "Do widzenia");
        dict.put("School", "Szkoła");
    }
// metoda
    public String translate(String wordToTranslate) {
        return dict.get(wordToTranslate);
    }
}